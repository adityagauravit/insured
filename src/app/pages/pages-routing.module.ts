import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { GalleryComponent } from './gallery/gallery.component';
import { AuthGuard } from '@/core/_guards/auth.guard';
import { NotFoundComponent } from './not-found/not-found.component';


const routes: Routes = [
  {
    path : '',
    component: HomepageComponent,

  },
  {
      path:'about-us',
      component:AboutUsComponent
  },
  {
    path: 'gallery',
    component:GalleryComponent,
    canActivate: [AuthGuard]        

  },
  {
    path: '**',
    component:NotFoundComponent

  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule {}
