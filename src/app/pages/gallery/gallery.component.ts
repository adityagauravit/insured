import { Component, OnInit } from '@angular/core';
import { GalleryFullviewComponent } from '@/components/gallery-fullview/gallery-fullview.component';
import { MatDialog } from '@angular/material/dialog';

interface Item {
  imageUrl:string;
  description:string;
}

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  galleryItems: Item[] = [
    {
      imageUrl:"https://via.placeholder.com/300/CCCCCC/808080",
      description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,"
    },
    {
      imageUrl:"https://via.placeholder.com/300/FFFF00/000000?Text=WebsiteBuilders.com",
      description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,"
    },
    {
      imageUrl:"https://via.placeholder.com/300/CCCCCC/808080",
      description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,"
    },
    {
      imageUrl:"https://via.placeholder.com/300/FFFF00/000000?Text=WebsiteBuilders.com",
      description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,"
    },
    {
      imageUrl:"https://via.placeholder.com/300/CCCCCC/808080",
      description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,"
    },
    {
      imageUrl:"https://via.placeholder.com/300/FFFF00/000000?Text=WebsiteBuilders.com",
      description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,"
    },
    {
      imageUrl:"https://via.placeholder.com/300/CCCCCC/808080",
      description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,"
    },
    {
      imageUrl:"https://via.placeholder.com/300/FFFF00/000000?Text=WebsiteBuilders.com",
      description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,"
    },
    {
      imageUrl:"https://via.placeholder.com/300/CCCCCC/808080",
      description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,"
    },
    {
      imageUrl:"https://via.placeholder.com/300/FFFF00/000000?Text=WebsiteBuilders.com",
      description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,"
    },
  ]
  constructor(
    private _dialog: MatDialog
 ) { }

  ngOnInit(): void {

  }

  openGalleryFullViewDialog(index:number, item: Item):void{
    const dialogMetaData = {};
    dialogMetaData['panelClass'] = 'gallery-fullview';
    dialogMetaData['disableClose'] = false;
    dialogMetaData['backdropClass'] = 'gallery-fullview-backdrop';

    dialogMetaData['data'] = {
      galleryItems: this.galleryItems,
      itemIndex:index
    };

    const dialogRef = this._dialog.open(GalleryFullviewComponent,dialogMetaData)
  }

}
