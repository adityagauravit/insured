import { Component, OnInit } from '@angular/core';

interface TeamMemeber {
  imageUrl: string;
  name: string;
  designation: string;
}

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  teamDetails:TeamMemeber[] = [
    {
      imageUrl: 'https://via.placeholder.com/150',
      name: 'loren ipsum',
      designation: 'loren ipsum',
    },

    {
      imageUrl: 'https://via.placeholder.com/150',
      name: 'loren ipsum',
      designation: 'loren ipsum',
    },
    {
      imageUrl: 'https://via.placeholder.com/150',
      name: 'loren ipsum',
      designation: 'loren ipsum',
    },
    {
      imageUrl: 'https://via.placeholder.com/150',
      name: 'loren ipsum',
      designation: 'loren ipsum',
    },
    {
      imageUrl: 'https://via.placeholder.com/150',
      name: 'loren ipsum',
      designation: 'loren ipsum',
    },
    {
      imageUrl: 'https://via.placeholder.com/150',
      name: 'loren ipsum',
      designation: 'loren ipsum',
    },
    {
      imageUrl: 'https://via.placeholder.com/150',
      name: 'loren ipsum',
      designation: 'loren ipsum',
    },
    {
      imageUrl: 'https://via.placeholder.com/150',
      name: 'loren ipsum',
      designation: 'loren ipsum',
    },
    {
      imageUrl: 'https://via.placeholder.com/150',
      name: 'loren ipsum',
      designation: 'loren ipsum',
    },
    {
      imageUrl: 'https://via.placeholder.com/150',
      name: 'loren ipsum',
      designation: 'loren ipsum',
    },
    {
      imageUrl: 'https://via.placeholder.com/150',
      name: 'loren ipsum',
      designation: 'loren ipsum',
    },
 ]
  constructor() { }

  ngOnInit(): void {
  }

}
