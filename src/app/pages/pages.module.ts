import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@/shared/shared.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ComponentsModule } from '@/components/components.module';
import { HomepageComponent } from './homepage/homepage.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { GalleryComponent } from './gallery/gallery.component';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
    declarations: [
        HomepageComponent,
        AboutUsComponent,
        GalleryComponent,
        NotFoundComponent

    ],

    imports : [
        CommonModule,
        RouterModule,
        PagesRoutingModule,
        SharedModule,
        ComponentsModule
    ],
    
    exports:[
       

    ],
    providers: [

    ]
})

export class PagesModule {

}