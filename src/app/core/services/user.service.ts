import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export class UserApiService {
  
    constructor(private http: HttpClient) {
       
    }
    
    getRegisteredUser() {
        return this.http.get(`assets/db/user.json`).subscribe(
            res => console.log(res)
        )
    }
}    