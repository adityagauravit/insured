import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { User } from '../_models/user';
import * as data from '../../../assets/db/user.json';
import { UserModel } from '../../../assets/db/user.model';

@Injectable({ providedIn: 'root' })

export class AuthenticationService {
    public currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    userObject: UserModel;
    userData:any;

    public isLoginSubject = new BehaviorSubject<boolean>(false);

    constructor(
    ) {
        this.userObject = new UserModel();
        this.userData = this.userObject.userData;
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
        this.isLoginSubject.asObservable();
        this.isLoginSubject.next(true);
    }

    signIn(userid: string, password: string) {
        console.log(userid);
        console.log(password);

        for(let i = 0; i < this.userData.length; i++){
            console.log(this.userData[i]['userid']);
             if(this.userData[i]['userid'] === userid){
               if( this.userData[i]['password'] === password ){
                 console.log('called');
                 localStorage.setItem('currentUser', JSON.stringify(this.userData[i]));
                 this.isLoginSubject.next(true);
                 return 'SUCCESSFUL';
               }
             }else{
               return 'FAILED'
   
             }
           }
    }
    
    public get currentUserValue(): User {
        if (localStorage.getItem('currentUser') !== (null || '')) {
            this.currentUserSubject.next( JSON.parse(localStorage.getItem('currentUser')) );
        } else {
            this.currentUserSubject.next(null);
        }
        console.log(this.currentUserSubject.value);
        return this.currentUserSubject.value;
    }
    

    signOut(): void {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
        localStorage.removeItem('is_logged_in');
        this.isLoginSubject.next(false);

    }

    isLoggedIn(): boolean {
        if (localStorage.getItem('currentUser')) {
            this.isLoginSubject.next(true);
        } else {
            this.isLoginSubject.next(false);
        }
        
        this.isLoginSubject.asObservable();
        return this.isLoginSubject.getValue();
    }

   
}