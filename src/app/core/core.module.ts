import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { WebModule } from '../web/web.module';
import { CoreRoutingModule } from './core-routing.module';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule,
        HttpClientModule,
        WebModule,
        CoreRoutingModule
    ],
    exports: [RouterModule],
    providers: [],
})
export class CoreModule {} 