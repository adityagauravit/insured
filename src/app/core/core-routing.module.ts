import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WebComponent } from '../web/web.component';

const routes: Routes = [
  {
    path : '',
    component: WebComponent,
     children: [
      {
        path: '',
        loadChildren: () => import('../pages/pages.module').then(m => m.PagesModule)
      }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class CoreRoutingModule {}
