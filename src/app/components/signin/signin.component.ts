import { Component, Inject, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '@/core/services/authentication.service';
import { map } from 'rxjs/operators';
import { UserApiService } from '@/core/services/user.service';
import * as data from './user.json';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
})

export class SigninComponent implements OnInit {
  error = '';

  constructor(
    private router: Router,
    private _authenticationService:AuthenticationService,
    private dialogRef: MatDialogRef<SigninComponent>,
  ) {
  }

  ngOnInit() {
  }

  userSigninForm = new FormGroup({
    userid: new FormControl ('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(25)])
  });

  submitSignInForm() {
      const currentPath = this.router.url;
      const postData = this.userSigninForm.value;

      const result = this._authenticationService.signIn(postData.userid,postData.password);
      
      if(result === 'FAILED') {
        this.error = 'Wrong Credentials';
        return;
      } else{
        this.closeTheDialog();
        window.location.reload();

      }
  }
  
  closeTheDialog() {
    this.dialogRef.close();
  }

}