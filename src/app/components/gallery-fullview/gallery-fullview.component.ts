import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

interface Item {
  imageUrl:string;
  description:string;
}

@Component({
  selector: 'app-gallery-fullview',
  templateUrl: './gallery-fullview.component.html',
  styleUrls: ['./gallery-fullview.component.scss']
})

export class GalleryFullviewComponent implements OnInit {
  galleryItems: Item[];
  itemIndex:number;
  item:Item;
  isDomLoaded:boolean = false;

  constructor(
    private dialogRef: MatDialogRef<GalleryFullviewComponent>,
    @Inject(MAT_DIALOG_DATA) data)  {
      this.galleryItems = data.galleryItems;
      this.itemIndex = data.itemIndex;
  }

  ngOnInit(): void {
    this.getGalleryItemOfIndex(this.itemIndex);
  }

  getGalleryItemOfIndex(index:number):Item {
    this.isDomLoaded = true;
    return this.item = this.galleryItems[index];
  }

  nextItem(): void{
    this.itemIndex = (this.itemIndex === this.galleryItems.length - 1) ? 0: this.itemIndex + 1;
    this.getGalleryItemOfIndex(this.itemIndex);
  }

  prevItem(): void{
    this.itemIndex = (this.itemIndex === 0) ? this.galleryItems.length - 1: this.itemIndex - 1;
    this.getGalleryItemOfIndex(this.itemIndex);
  }

  closeTheGalleryDialog():void{
      this.dialogRef.close();
  }


}
