import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryFullviewComponent } from './gallery-fullview.component';

describe('GalleryFullviewComponent', () => {
  let component: GalleryFullviewComponent;
  let fixture: ComponentFixture<GalleryFullviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalleryFullviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryFullviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
