import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@/shared/shared.module';
import { SigninComponent } from './signin/signin.component';
import { GalleryFullviewComponent } from './gallery-fullview/gallery-fullview.component';

@NgModule({
    declarations: [
        SigninComponent,
        GalleryFullviewComponent
    ],
    imports : [
        CommonModule,
        SharedModule
    ],
    exports:[],
    providers: [],
    entryComponents:[ 
        SigninComponent,
        GalleryFullviewComponent
    ]
})

export class ComponentsModule {

}