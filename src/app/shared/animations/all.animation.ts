import { sequence, trigger, animate, style, group, query,
    transition,
    animateChild,
    state,
    animation,
    useAnimation,
    stagger
  } from '@angular/animations';

const customAnimation = animation([
    style({
        opacity: '{{opacity}}',
        transform: 'scale({{scale}}) translate3d( {{x}}, {{y}}, {{z}} )'

    }),
    animate('{{duration}} {{delay}} cubic-bezier(0.0, 0.0, 0.2, 1)', style('*'))
],
 {
     params: {
        opacity: '0',
        scale:  '1',
        delay: '0ms',
        duration: '200ms',
        x: '0',
        y: '0',
        z: '0'

     }
});

export const AllAnimation = [
    trigger('animate', [transition('void => *', useAnimation(customAnimation))]),
   trigger('visibilityInOut', [
        state('0', style({
            visibility: 'hidden',
           opacity: 0
        })),
        state('1', style({
            visibility: 'visible',
            opacity: 1
        })),
        transition('1 => 0', animate('1000ms ease-out')),
        transition('0 => 1', animate('1000ms ease-in')),

    ]),

   
];
