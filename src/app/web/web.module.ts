import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@/shared/shared.module';
import { ComponentsModule } from '@/components/components.module';
import { WebComponent } from './web.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
    declarations: [
        WebComponent,
        HeaderComponent,
        FooterComponent,
    ],

    imports : [
        CommonModule,
        RouterModule,
        SharedModule,
        ComponentsModule
    ],
    
    exports:[
        HeaderComponent,
        FooterComponent
    ],
    providers: []
})

export class WebModule {

}