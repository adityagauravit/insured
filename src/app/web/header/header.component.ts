import {  Component, OnInit,ElementRef,  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Event, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { BreakpointObserver, Breakpoints , BreakpointState} from '@angular/cdk/layout';
import { MatDialog } from '@angular/material/dialog';
import { MatSidenav } from '@angular/material/sidenav';
import { SigninComponent } from '@/components/signin/signin.component';
import { AuthenticationService } from '@services/authentication.service';
import { AllAnimation } from '@/shared/animations/all.animation';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [AllAnimation],
})

export class HeaderComponent implements OnInit {
  isDomLoaded: boolean = false;
  isHandset:boolean;
  isOpen = false;
  currentUser:any;  
  isLogged = this.authenticationService.isLoggedIn();
  @ViewChild(MatSidenav, {static: false}) sidenav: MatSidenav;
  
  constructor(
    private _elementRef: ElementRef,
    private _breakpointObserver: BreakpointObserver,
    private _dialog: MatDialog,
    private authenticationService: AuthenticationService,
    private _router:Router,
    private _activatedRoute: ActivatedRoute
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
      console.log(this.currentUser);
      console.log('isLogged$:' + this.isLogged);

      const layoutChanges = this._breakpointObserver.observe([Breakpoints.Handset]);
      
      layoutChanges.subscribe(result => {
        this.updateMyLayoutForOrientationChange(result);
      });
      // this.trackNavigationInstance();

   }
   
  ngOnInit(): void {
    this.trackNavigationInstance();
  }

  trackNavigationInstance():void{

    this._router.events.subscribe((event: Event) => {
      
      if (event instanceof NavigationStart) {
        this.isDomLoaded = false; 
      }

      if (event instanceof NavigationEnd) {
        this.sidenav.close();
        this.isDomLoaded = true;
        console.log('called');
       }

      if (event instanceof NavigationError) {
        console.log(event.error);
      }

    })
  } 

  toggleDropdown() {
    console.log('called');
    this.isOpen = !this.isOpen;
  }

  updateMyLayoutForOrientationChange(result){
      this.isHandset = result.matches;
      console.log(this.isHandset);
  }
 
  userSignIn(){
    const dialogMetaData = {};
    dialogMetaData['panelClass'] = 'signin-dialog';
    dialogMetaData['disableClose'] = false;
    dialogMetaData['backdropClass'] = 'signin-dialog-backdrop';

    const dialogRef = this._dialog.open(SigninComponent,dialogMetaData)
  }  

  logout() {
    this.authenticationService.signOut();
    window.location.reload();
    // this._router.navigate(['/']);
 }

}
